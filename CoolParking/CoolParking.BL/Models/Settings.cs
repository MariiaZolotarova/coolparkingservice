﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal FirstBalance => 0;
        public static int ParkingCapacity => 10;
        public static double PeriodOfPaid => 5;
        public static double LogInterval => 60;
        public static decimal FineCoefficient => 2.5M;
        public static Dictionary<VehicleType, decimal> VehicleDictionary = new Dictionary<VehicleType, decimal>()
        {
             {VehicleType.PassengerCar,2},
             {VehicleType.Truck,5},
             {VehicleType.Bus,3.5M},
             {VehicleType.Motorcycle,1}
        };
    }
}