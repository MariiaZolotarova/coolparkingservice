﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonConstructor]
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance <= 0)
            {
                throw new ArgumentException();
            }

            if (!IsIdValid(id))
            {
                throw new ArgumentException();
            }

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
            RegisteredPlateNumber.Add(id);
        }

        public static bool IsIdValid(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return false;
            }

            return Regex.IsMatch(id, @"\A[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]\Z");
        }

        [JsonPropertyName("id")]
        public string Id { get; }

        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var random = new Random();
            string vehicleId;

            while (true)
            {
                vehicleId = $"{(char)random.Next(65, 90)}{(char)random.Next(65, 90)}-{random.Next(0, 9)}{random.Next(0, 9)}{random.Next(0, 9)}{random.Next(0, 9)}-{(char)random.Next(65, 90)}{(char)random.Next(65, 90)}";

                if (RegisteredPlateNumber.All(x => x != vehicleId))
                {
                    RegisteredPlateNumber.Add(vehicleId);
                    break;
                }
            }

            return vehicleId;
        }

        private static readonly List<string> RegisteredPlateNumber = new List<string>();
    }

}