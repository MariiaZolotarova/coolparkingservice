﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{

    public sealed class Parking
    {
        public decimal ParkingBalance;
        public List<Vehicle> VehicleCollection = new List<Vehicle>();
        public List<TransactionInfo> TransactionCollection = new List<TransactionInfo>();

        private static readonly Lazy<Parking> instance = new Lazy<Parking>(() => new Parking());

        private Parking()
        {
            ParkingBalance = Settings.FirstBalance;
        }

        public static Parking Instance => instance.Value;
    }
}