﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;
namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer Timer { get; set; }
        public double Interval { get; set; }

        public TimerService(double interval)
        {
            Interval = interval;
        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            Timer.Close();
        }

        public void Start()
        {
            Timer = new Timer(Interval * 1000);
            Timer.Elapsed += Elapsed;
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }
    }
}