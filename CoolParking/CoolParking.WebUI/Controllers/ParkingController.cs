﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebUI.Controllers
{
    public class ParkingController : Controller
    {
        public async Task<IActionResult> Index()
        {
            using var client = new HttpClient();

            var capacityResponse = await client.GetAsync("https://localhost:44374/api/parking/capacity");
            ViewBag.Capacity = await capacityResponse.Content.ReadAsStringAsync();

            var balanceResponse = await client.GetAsync("https://localhost:44374/api/parking/balance");
            ViewBag.Balance = await balanceResponse.Content.ReadAsStringAsync();

            var freePlacesResponse = await client.GetAsync("https://localhost:44374/api/parking/freePlaces");
            ViewBag.FreePlaces = await freePlacesResponse.Content.ReadAsStringAsync();

            return View();
        }
    }
}