﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebUI.Controllers
{
    public class TransactionsController : Controller
    {
        public async Task<IActionResult> Index()
        {
            using var client = new HttpClient();

            var allResponse = await client.GetAsync("https://localhost:44374/api/transactions/all");
            if (allResponse.StatusCode == HttpStatusCode.NotFound)
            {
                ViewBag.AllTransactions = "Транзакцій не знайдено";
            }
            else
            {
                ViewBag.AllTransactions = await allResponse.Content.ReadAsStringAsync();
            }
            return View();
        }

        public async Task<IActionResult> LastTransactions()
        {
            using var client = new HttpClient();

            var lastResponse = await client.GetAsync("https://localhost:44374/api/transactions/last");
            var model = JsonConvert.DeserializeObject<List<TransactionInfo>>(await lastResponse.Content.ReadAsStringAsync());
            return View(model);
        }
    }
}