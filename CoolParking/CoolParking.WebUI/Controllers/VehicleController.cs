﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebUI.Controllers
{
    public class VehicleController : Controller
    {
        public async Task<IActionResult> List()
        {
            using var client = new HttpClient();

            var vehicleResponse = await client.GetAsync("https://localhost:44374/api/vehicles");
            var model = JsonConvert.DeserializeObject<List<Vehicle>>(await vehicleResponse.Content.ReadAsStringAsync());
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,VehicleType,Balance")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                using var client = new HttpClient();

                var json = JsonConvert.SerializeObject(vehicle);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var addVehicleURL = "https://localhost:44374/api/vehicles";
                var response = await client.PostAsync(addVehicleURL, data);
                if (response.StatusCode== HttpStatusCode.Created)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.ErrorMessage = "Парковку заповнено, або машина з такими номерами вже існує";
                    return View();
                }
            }

            return View(vehicle);
        }

        public async Task<IActionResult> Delete(string id)
        {
            using var client = new HttpClient();

            var vehicleDelete = await client.GetAsync($"https://localhost:44374/api/vehicles/{id}");
            var model = JsonConvert.DeserializeObject<Vehicle>(await vehicleDelete.Content.ReadAsStringAsync());
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteId(string id)
        {
            using var client = new HttpClient();

            var vehicleDelete = $"https://localhost:44374/api/vehicles/{id}";
            var response = await client.DeleteAsync(vehicleDelete);

            return RedirectToAction("List");
        }

        public IActionResult TopUp(string id)
        {
            return View(new TopUpModel
            {
                Id = id,
                Sum = 0
            });
        }

        [HttpPost]
        public async Task<IActionResult> TopUpVehicle(TopUpModel topUpModel)
        {
            using var client = new HttpClient();

            var json = JsonConvert.SerializeObject(topUpModel);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var addVehicleURL = "https://localhost:44374/api/transactions/topUpVehicle";
            var response = await client.PutAsync(addVehicleURL, data);
            return RedirectToAction("List");
        }
    }
}