﻿using System;
using Newtonsoft.Json;

namespace CoolParking.WebUI.Models
{
    public class TransactionInfo
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime Time { get; set; }
    }
}