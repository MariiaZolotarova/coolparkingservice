﻿namespace CoolParking.WebUI.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}