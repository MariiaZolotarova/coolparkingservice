﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace CoolParking.WebUI.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        [RegularExpression(@"\A[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]\Z")]
        [Required]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        [Required]
        public decimal Balance { get; set; }
    }
}