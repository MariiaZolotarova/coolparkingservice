﻿using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class TopUpModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}