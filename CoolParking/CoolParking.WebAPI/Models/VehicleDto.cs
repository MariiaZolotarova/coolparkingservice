﻿using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class VehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}