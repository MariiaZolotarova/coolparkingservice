﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("last")]
        public TransactionInfoDto[] LastTransactions()
        {
            return parkingService.GetLastParkingTransactions().Select(x => new TransactionInfoDto
            {
                Sum = x.Sum,
                Time = x.Time,
                VehicleId = x.VehicleId
            }).ToArray();
        }

        [HttpGet("all")]
        public IActionResult AllTransactions()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch (InvalidOperationException exception)
            {
                return NotFound();
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult Put([FromBody] TopUpModel topUpModel)
        {
            if (topUpModel == null || !Vehicle.IsIdValid(topUpModel.Id) || topUpModel.Sum <= 0)
            {
                return BadRequest();
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == topUpModel.Id))
            {
                return NotFound();
            }

            parkingService.TopUpVehicle(topUpModel.Id, topUpModel.Sum);

            return Ok(parkingService.GetVehicles().First(x => x.Id == topUpModel.Id));
        }

    }
}