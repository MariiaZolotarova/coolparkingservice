﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Controllers
{
    public class TransactionInfoDto
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime Time { get; set; }
    }
}
