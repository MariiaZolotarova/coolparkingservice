﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parkingService.GetVehicles();
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id))
            {
                return BadRequest();
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                return NotFound();
            }

            Vehicle vehicle = parkingService.GetVehicles().First(x => x.Id == id);

            return Ok(vehicle);
        }

        [HttpPost]
        public IActionResult GetVehicles([FromBody] VehicleDto vehicleDto)
        {
            if (vehicleDto == null)
            {
                return BadRequest();
            }

            if (!Vehicle.IsIdValid(vehicleDto.Id) || vehicleDto.Balance <= 0 || vehicleDto.VehicleType < 0 || (int)vehicleDto.VehicleType > 3)
            {
                return BadRequest();
            }

            if (parkingService.GetVehicles().Any(x => x.Id == vehicleDto.Id))
            {
                return BadRequest();
            }

            var vehicle = new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);

            parkingService.AddVehicle(vehicle);

            return StatusCode(201, vehicle);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (!Vehicle.IsIdValid(id))
            {
                return BadRequest();
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                return NotFound();
            }

            parkingService.RemoveVehicle(id);

            return NoContent();
        }
    }
}